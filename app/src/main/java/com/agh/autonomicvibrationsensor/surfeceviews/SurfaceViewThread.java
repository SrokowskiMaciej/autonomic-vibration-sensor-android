package com.agh.autonomicvibrationsensor.surfeceviews;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class SurfaceViewThread extends Thread {

    private final static int SLEEP_TIME = 40;

    private boolean running = false;
    private BaseSurfaceView surfaceView = null;
    private SurfaceHolder surfaceHolder = null;

    public SurfaceViewThread(BaseSurfaceView surfaceView) {
        super();
        this.surfaceView = surfaceView;
        this.surfaceHolder = surfaceView.getHolder();
    }

    public void startThread() {
        running = true;
        super.start();
    }


    public void stopThread() {
        running = false;
    }

    public void run() {
        Canvas c = null;
        while (running) {
            c = null;
            try {
                c = surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    if (c != null) {
                        surfaceView.doDraw(c);
                    }
                }
                sleep(SLEEP_TIME);
            } catch (InterruptedException ie) {
            } finally {
                // do this in a finally so that if an exception is thrown
                // we don't leave the Surface in an inconsistent state
                if (c != null) {
                    surfaceHolder.unlockCanvasAndPost(c);
                }
            }

        }
    }
}
