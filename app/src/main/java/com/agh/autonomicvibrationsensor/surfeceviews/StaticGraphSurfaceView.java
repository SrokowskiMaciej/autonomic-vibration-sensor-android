package com.agh.autonomicvibrationsensor.surfeceviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.SurfaceHolder;

import com.agh.autonomicvibrationsensor.model.ADXL345Settings;
import com.agh.autonomicvibrationsensor.model.Utilities;

import java.util.ArrayList;

import gnu.trove.list.array.TFloatArrayList;

/**
 * Created by Maciek on 2014-08-30.
 */
public class StaticGraphSurfaceView extends BaseSurfaceView {

    //Minimum shown amplitude in m/s^2
    private final static int MIN_SHOWN_AMPLITUDE = 20;

    //Number of samples shown
    private int samplesNumber = 0;

    //Paints used to draw on Canvas
    private final static Paint mPointsPaint;
    private final static Paint mHorizontalLinesPaint;
    private final static Paint mVerticalLinesPaint;
    private final static Paint mBlankPaint;
    private final static Paint mTextPaint;
    private final static Paint mBoundsPaint;

    //Margins of the graph on Canvas
    private final static float TEXT_HEIGHT = 25;
    private final static float TOP_MARGIN = 20;
    private final static float LEFT_MARGIN = 65;
    private final static float RIGHT_MARGIN = 20;
    private final static float BOTTOM_MARGIN = 35;

    //  private final static float multiplicator=range/resolution;


    public StaticGraphSurfaceView(Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    public StaticGraphSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
    }

    public StaticGraphSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getHolder().addCallback(this);
    }

    //Initialization of Paints
    static {
        mPointsPaint = new Paint();
        mPointsPaint.setColor(Color.RED);
        mPointsPaint.setStyle(Paint.Style.STROKE);
        mPointsPaint.setStrokeWidth(3.0f);
        mHorizontalLinesPaint = new Paint();
        mHorizontalLinesPaint.setColor(Color.BLUE);
        mHorizontalLinesPaint.setStyle(Paint.Style.STROKE);
        mHorizontalLinesPaint.setStrokeWidth(2.0f);
        mHorizontalLinesPaint.setPathEffect(new DashPathEffect(new float[]{10, 20}, 0));
        mVerticalLinesPaint = new Paint();
        mVerticalLinesPaint.setColor(Color.RED);
        mVerticalLinesPaint.setStyle(Paint.Style.STROKE);
        mVerticalLinesPaint.setStrokeWidth(2.0f);
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(TEXT_HEIGHT);
        mBlankPaint = new Paint();
        mBlankPaint.setColor(Color.WHITE);
        mBlankPaint.setStyle(Paint.Style.FILL);
        mBoundsPaint = new Paint();
        mBoundsPaint.setColor(Color.BLACK);
        mBoundsPaint.setStyle(Paint.Style.STROKE);
        mBoundsPaint.setStrokeWidth(3.0f);
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        startThread();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopThread();
    }

    public void doDraw(Canvas canvas) {
        //Draw blank on canvas
        canvas.drawColor(getContext().getResources().getColor(android.R.color.white));
        //Save canvas to restore its dimensions after drawing
        canvas.save();
        //Scale canvas to meet the size of a graph. This means without margins
        float scaleX = 1 - (LEFT_MARGIN + RIGHT_MARGIN) / (float) canvas.getWidth();
        float scaleY = 1 - (TOP_MARGIN + BOTTOM_MARGIN) / (float) canvas.getHeight();
        canvas.scale(scaleX, scaleY, 0, 0);
        //Translate canvas to meet the position of a graph.
        float translateX = LEFT_MARGIN;
        float translateY = TOP_MARGIN + canvas.getHeight() / 2;
        canvas.translate(translateX, translateY);

        TFloatArrayList drawnResults;
        float shownMaxAmplitude;
        //Get samples and maxShownAmplitude for current Axis
        switch (getAxis()) {
            case X:
                drawnResults = getMeasurement().getXAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestXAxisSample();
                break;
            case Y:
                drawnResults = getMeasurement().getYAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestYAxisSample();
                break;
            case Z:
                drawnResults = getMeasurement().getZAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestZAxisSample();
                break;
            default:
                drawnResults = getMeasurement().getXAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestXAxisSample();
        }
        //Find amplitude  to show. It should be more than 20 and rounded to the nearest 10
        if (shownMaxAmplitude < MIN_SHOWN_AMPLITUDE)
            shownMaxAmplitude = MIN_SHOWN_AMPLITUDE;
        int range = Utilities.roundUpToNearest(shownMaxAmplitude, 10) * 2;
        //Get number of samples
        synchronized (getMeasurement().getLock()) {
            samplesNumber = drawnResults.size();
        }
        //Find number of pixels per sampling period and per quant
        float pixelsPerQuant = ((float) canvas.getHeight()) / range;
        float pixelsPerPeriod = ((float) canvas.getWidth()) / (samplesNumber);

        //Draw
        drawResults(canvas, drawnResults, getMeasurement().getLock(), pixelsPerQuant, pixelsPerPeriod);
        drawVerticalLines(canvas, pixelsPerPeriod, samplesNumber);
        drawTimeDescriptions(canvas, pixelsPerPeriod, samplesNumber);
        drawHorizontalLines(canvas);
        drawValuesDescriptions(canvas, range);
        drawBounds(canvas);
        //Restore canvas dimensions
        canvas.restore();

    }

    /**
     * Draws results on a canvas
     *
     * @param canvas          Canvas to draw on
     * @param results         Results to draw
     * @param lock            lock to synchronize reading results against
     * @param pixelsPerQuant  number of pixels per quant
     * @param pixelsPerPeriod number on pixels per sampling period
     */
    private void drawResults(Canvas canvas, TFloatArrayList results, Object lock, float pixelsPerQuant, float pixelsPerPeriod) {

        synchronized (lock) {
            for (int i = 0; i < samplesNumber - 1; i++) {
                float xStart = i * pixelsPerPeriod;
                float yStart = results.get(i) * pixelsPerQuant;
                float xStop = (i + 1) * pixelsPerPeriod;
                float yStop = results.get(i + 1) * pixelsPerQuant;
                canvas.drawLine(xStart, yStart, xStop, yStop, mPointsPaint);
            }
        }
    }

    /**
     * Draws five horizontal lines indicating levels on m/s^2
     *
     * @param canvas Canvas to draw on
     */
    private void drawHorizontalLines(Canvas canvas) {
        float[] horizontalGridLinesPoints = new float[20];

        for (int i = 0; i <= 4; i++) {
            horizontalGridLinesPoints[4 * i] = 0;
            horizontalGridLinesPoints[4 * i + 1] = ((float) canvas.getHeight()) * (((float) i - 2) / 4.0f);
            horizontalGridLinesPoints[4 * i + 2] = canvas.getWidth();
            horizontalGridLinesPoints[4 * i + 3] = ((float) canvas.getHeight()) * (((float) i - 2) / 4.0f);

        }
        canvas.drawLines(horizontalGridLinesPoints, mHorizontalLinesPaint);
    }

    /**
     * Draws descriptions to lines indicating levels on m/s^2
     *
     * @param canvas Canvas to draw on
     */
    private void drawValuesDescriptions(Canvas canvas, int range) {
        //Draw blank space where descriptions should be to cover moving vertical lines
        canvas.drawRect(-LEFT_MARGIN, -canvas.getHeight() / 2, -1, canvas.getHeight() / 2 + BOTTOM_MARGIN, mBlankPaint);
        //Draw descriptions
        for (int i = -2; i <= 2; i++) {
            float description = ((float) range) * ((float) i) / 4.0f;
            float yPosition = -canvas.getHeight() * ((float) i) / 4.0f + TEXT_HEIGHT / 2;
            float xPosition = -LEFT_MARGIN + 5;
            canvas.drawText(String.valueOf(description), xPosition, yPosition, mTextPaint);
        }
    }

    /**
     * Draws line every second of measurement
     *
     * @param canvas          Canvas to draw on
     * @param pixelsPerPeriod number on pixels per sampling period
     * @param samplesNumber   number of samples in current results
     */
    private void drawVerticalLines(Canvas canvas, float pixelsPerPeriod, int samplesNumber) {
        int fullSecondsShown = samplesNumber / ADXL345Settings.samplingFrequency;

        float[] verticalGridLinesPoints = new float[fullSecondsShown * 4 + 4];
        for (int i = 0; i <= fullSecondsShown; i++) {
            verticalGridLinesPoints[4 * i] = (i * ADXL345Settings.samplingFrequency) * pixelsPerPeriod;
            verticalGridLinesPoints[4 * i + 1] = -canvas.getHeight() / 2;
            verticalGridLinesPoints[4 * i + 2] = (i * ADXL345Settings.samplingFrequency) * pixelsPerPeriod;
            verticalGridLinesPoints[4 * i + 3] = canvas.getHeight() / 2;

        }

        
    }





        /**
         * Draw time description every second of measurement
         * @param canvas Canvas to draw on
         * @param pixelsPerPeriod Number on pixels per sampling period
         * @param samplesNumber Number of samples
         */
    private void drawTimeDescriptions(Canvas canvas, float pixelsPerPeriod, int samplesNumber) {
        int fullSecondsShown = samplesNumber / ADXL345Settings.samplingFrequency;

        for (int i = 0; i <= fullSecondsShown; i++) {

            String description = String.valueOf(i) + " s";
            float yPosition = canvas.getHeight() / 2 + 25;
            float xPosition = (i * ADXL345Settings.samplingFrequency) * pixelsPerPeriod;
            canvas.drawText(description, xPosition, yPosition, mTextPaint);
        }
    }

    /**
     * Draw bounds around Graph
     *
     * @param canvas
     */
    private void drawBounds(Canvas canvas) {
        canvas.drawRect(0, -canvas.getHeight() / 2, canvas.getWidth(), canvas.getHeight() / 2, mBoundsPaint);
    }
}
