package com.agh.autonomicvibrationsensor.surfeceviews;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.agh.autonomicvibrationsensor.model.Axis;
import com.agh.autonomicvibrationsensor.model.Measurement;

/**
 * Base surfaceView.
 */
public abstract class BaseSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceViewThread mThread;
    private Measurement mMeasurement = new Measurement();
    private Axis mDrawnAxis = Axis.X;

    public BaseSurfaceView(Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    public BaseSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
    }

    public BaseSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getHolder().addCallback(this);
    }

    /**
     * Sets measurements from which results surface will draw
     *
     * @param measurement new Measurement
     */
    public void setMeasurement(Measurement measurement) {
        this.mMeasurement = measurement;
    }

    /**
     * Get current measurement
     */
    public Measurement getMeasurement() {
        return mMeasurement;
    }

    /**
     * Set axis on which surface will draw results from current measurement
     *
     * @param axis
     */
    public void setAxis(Axis axis) {
        mDrawnAxis = axis;
    }

    /**
     * Get current Axis
     *
     * @return current Axis
     */
    public Axis getAxis() {
        return mDrawnAxis;
    }

    /**
     * Abstract doDraw method. Called from SurfaceViewThread
     *
     * @param canvas Canvas on which surface will draw
     */
    public abstract void doDraw(Canvas canvas);

    /**
     * Stops thread drawing on Surface
     */
    protected void stopThread() {
        if (mThread != null) {
            mThread.stopThread();
            // Waiting for the thread to die by calling thread.join,
            // repeatedly if necessary
            boolean retry = true;
            while (retry) {
                try {
                    mThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
            mThread = null;
        }
    }

    /**
     * Starts thread drawing on Surface
     */
    protected void startThread() {
        if (mThread == null) {
            mThread = new SurfaceViewThread(this);
            mThread.startThread();
        }
    }

}
