package com.agh.autonomicvibrationsensor.surfeceviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;

import com.agh.autonomicvibrationsensor.model.ADXL345Settings;
import com.agh.autonomicvibrationsensor.model.Utilities;

import gnu.trove.list.array.TFloatArrayList;

/**
 * Implementation of surfaceView used for showing results of measurement while retrieving them from bluetooth device
 */
public class DynamicGraphSurfaceView extends BaseSurfaceView {

    //Number of last seconds to show on current measurement
    private final static int LAST_SECONDS_TO_SHOW = 5;
    //Minimum shown amplitude in m/s^2
    private final static int MIN_SHOWN_AMPLITUDE = 20;
    //Last shown sample number
    private int currentSampleNumber = 0;

    //Paints used to draw on Canvas
    private final static Paint mPointsPaint;
    private final static Paint mHorizontalLinesPaint;
    private final static Paint mVerticalLinesPaint;
    private final static Paint mBlankPaint;
    private final static Paint mTextPaint;
    private final static Paint mBoundsPaint;

    //Margins of the graph on Canvas
    private final static float TEXT_HEIGHT = 25;
    private final static float TOP_MARGIN = 20;
    private final static float LEFT_MARGIN = 65;
    private final static float RIGHT_MARGIN = 20;
    private final static float BOTTOM_MARGIN = 35;

    public DynamicGraphSurfaceView(Context context) {
        super(context);
        getHolder().addCallback(this);
    }

    public DynamicGraphSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
    }

    public DynamicGraphSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getHolder().addCallback(this);
    }

    //Initialization of Paints
    static {
        mPointsPaint = new Paint();
        mPointsPaint.setColor(Color.RED);
        mPointsPaint.setStyle(Paint.Style.STROKE);
        mPointsPaint.setStrokeWidth(3.0f);
        mHorizontalLinesPaint = new Paint();
        mHorizontalLinesPaint.setColor(Color.BLUE);
        mHorizontalLinesPaint.setStyle(Paint.Style.STROKE);
        mHorizontalLinesPaint.setStrokeWidth(2.0f);
        mHorizontalLinesPaint.setPathEffect(new DashPathEffect(new float[]{10, 20}, 0));
        mVerticalLinesPaint = new Paint();
        mVerticalLinesPaint.setColor(Color.RED);
        mVerticalLinesPaint.setStyle(Paint.Style.STROKE);
        mVerticalLinesPaint.setStrokeWidth(2.0f);
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(TEXT_HEIGHT);
        mBlankPaint = new Paint();
        mBlankPaint.setColor(Color.WHITE);
        mBlankPaint.setStyle(Paint.Style.FILL);
        mBoundsPaint = new Paint();
        mBoundsPaint.setColor(Color.BLACK);
        mBoundsPaint.setStyle(Paint.Style.STROKE);
        mBoundsPaint.setStrokeWidth(3.0f);
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startThread();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopThread();
    }


    public void doDraw(Canvas canvas) {
        //Draw blank on canvas
        canvas.drawColor(getContext().getResources().getColor(android.R.color.white));
        //Save canvas to restore its dimensions after drawing
        canvas.save();
        //Scale canvas to meet the size of a graph. This means without margins
        float scaleX = 1 - (LEFT_MARGIN + RIGHT_MARGIN) / (float) canvas.getWidth();
        float scaleY = 1 - (TOP_MARGIN + BOTTOM_MARGIN) / (float) canvas.getHeight();
        canvas.scale(scaleX, scaleY, 0, 0);
        //Translate canvas to meet the position of a graph.
        float translateX = LEFT_MARGIN;
        float translateY = TOP_MARGIN + canvas.getHeight() / 2;
        canvas.translate(translateX, translateY);

        TFloatArrayList drawnResults;
        float shownMaxAmplitude;
        //Get samples and maxShownAmplitude for current Axis
        switch (getAxis()) {
            case X:
                drawnResults = getMeasurement().getXAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestXAxisSample();
                break;
            case Y:
                drawnResults = getMeasurement().getYAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestYAxisSample();
                break;
            case Z:
                drawnResults = getMeasurement().getZAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestZAxisSample();
                break;
            default:
                drawnResults = getMeasurement().getXAxisResults();
                shownMaxAmplitude = getMeasurement().getAbsHighestXAxisSample();
        }
        //Find amplitude  to show. It should be more than 20 and rounded to the nearest 10
        if (shownMaxAmplitude < MIN_SHOWN_AMPLITUDE)
            shownMaxAmplitude = MIN_SHOWN_AMPLITUDE;
        int range = Utilities.roundUpToNearest(shownMaxAmplitude, 10) * 2;

        //Find number of pixels per sampling period and per quant
        float pixelsPerQuant = ((float) canvas.getHeight()) / range;
        float pixelsPerPeriod = ((float) canvas.getWidth()) / (LAST_SECONDS_TO_SHOW * ADXL345Settings.samplingFrequency);
        //Draw
        drawResults(canvas, drawnResults, getMeasurement().getLock(), pixelsPerQuant, pixelsPerPeriod);
        drawVerticalLines(canvas, pixelsPerPeriod, currentSampleNumber);
        drawTimeDescriptions(canvas, pixelsPerPeriod, currentSampleNumber);
        drawHorizontalLines(canvas);
        drawValuesDescriptions(canvas, range);
        drawBounds(canvas);
        //Restore canvas dimensions
        canvas.restore();

    }

    /**
     * Draws results on a canvas
     *
     * @param canvas          Canvas to draw on
     * @param results         Results to draw
     * @param lock            lock to synchronize reading results against
     * @param pixelsPerQuant  number of pixels per quant
     * @param pixelsPerPeriod number on pixels per sampling period
     */
    private void drawResults(Canvas canvas, TFloatArrayList results, Object lock, float pixelsPerQuant, float pixelsPerPeriod) {
        synchronized (lock) {
            //Find how many samples are excluded from drawing because we are only drawing last x seconds
            int invisibleSamplesNumber = 0;
            if (results.size() >= LAST_SECONDS_TO_SHOW * ADXL345Settings.samplingFrequency)
                invisibleSamplesNumber = results.size() - LAST_SECONDS_TO_SHOW * ADXL345Settings.samplingFrequency;
            // Get number of samples globally for drawing vertical lines
            currentSampleNumber = results.size();
            //Draw results
            for (int i = invisibleSamplesNumber + 1; i < currentSampleNumber; i++) {
                float xStart = ((float) (i - invisibleSamplesNumber - 1)) * pixelsPerPeriod;
                float yStart = results.get(i - 1) * pixelsPerQuant;
                float xStop = ((float) (i - invisibleSamplesNumber)) * pixelsPerPeriod;
                float yStop = results.get(i) * pixelsPerQuant;
                canvas.drawLine(xStart, yStart, xStop, yStop, mPointsPaint);
            }
        }
    }


    /**
     * Draws five horizontal lines indicating levels on m/s^2
     *
     * @param canvas Canvas to draw on
     */
    private void drawHorizontalLines(Canvas canvas) {
        float[] horizontalGridLinesPoints = new float[20];
        for (int i = 0; i <= 4; i++) {
            horizontalGridLinesPoints[4 * i] = 0;
            horizontalGridLinesPoints[4 * i + 1] = ((float) canvas.getHeight()) * (((float) i - 2) / 4.0f);
            horizontalGridLinesPoints[4 * i + 2] = canvas.getWidth();
            horizontalGridLinesPoints[4 * i + 3] = ((float) canvas.getHeight()) * (((float) i - 2) / 4.0f);

        }
        canvas.drawLines(horizontalGridLinesPoints, mHorizontalLinesPaint);
    }

    /**
     * Draws descriptions to lines indicating levels on m/s^2
     *
     * @param canvas Canvas to draw on
     */
    private void drawValuesDescriptions(Canvas canvas, int range) {
        //Draw blank space where descriptions should be to cover moving vertical lines
        canvas.drawRect(-LEFT_MARGIN, -canvas.getHeight() / 2, -1, canvas.getHeight() / 2 + BOTTOM_MARGIN, mBlankPaint);
        //Draw descriptions
        for (int i = -2; i <= 2; i++) {
            float description = ((float) range) * ((float) i) / 4.0f;
            float yPosition = -canvas.getHeight() * ((float) i) / 4.0f + TEXT_HEIGHT / 2;
            float xPosition = -LEFT_MARGIN + 5;
            canvas.drawText(String.valueOf(description), xPosition, yPosition, mTextPaint);
        }
    }

    /**
     * Draws line every second of measurement
     *
     * @param canvas          Canvas to draw on
     * @param pixelsPerPeriod Number on pixels per sampling period
     * @param samplesNumber   Number of samples in current results
     */
    private void drawVerticalLines(Canvas canvas, float pixelsPerPeriod, int samplesNumber) {

        int samplesInCurrentSecond = 0;
        //Find number of samples that is in th bounds of current second
        if (samplesNumber > LAST_SECONDS_TO_SHOW * ADXL345Settings.samplingFrequency) {
            int samplesInFullShownSeconds = Utilities.roundDownToNearest((float) samplesNumber, ADXL345Settings.samplingFrequency);
            samplesInCurrentSecond = samplesNumber - samplesInFullShownSeconds;
        }
        //Draw lines
        float[] verticalGridLinesPoints = new float[LAST_SECONDS_TO_SHOW * 4 + 4];
        for (int i = 0; i <= 5; i++) {
            verticalGridLinesPoints[4 * i] = (i * ADXL345Settings.samplingFrequency - samplesInCurrentSecond) * pixelsPerPeriod;
            verticalGridLinesPoints[4 * i + 1] = -canvas.getHeight() / 2;
            verticalGridLinesPoints[4 * i + 2] = (i * ADXL345Settings.samplingFrequency - samplesInCurrentSecond) * pixelsPerPeriod;
            verticalGridLinesPoints[4 * i + 3] = canvas.getHeight() / 2;

        }
        canvas.drawLines(verticalGridLinesPoints, mVerticalLinesPaint);
    }

    /**
     * Draw time description every second of measurement
     * @param canvas Canvas to draw on
     * @param pixelsPerPeriod Number on pixels per sampling period
     * @param samplesNumber Number of samples
     */
    private void drawTimeDescriptions(Canvas canvas, float pixelsPerPeriod, int samplesNumber) {
        int invisibleSeconds = 0;
        int samplesInCurrentSecond = 0;
        //Find number of samples that is in th bounds of current second and how many second are off the screen
        if (samplesNumber > LAST_SECONDS_TO_SHOW * ADXL345Settings.samplingFrequency) {
            int samplesInFullShownSeconds = Utilities.roundDownToNearest((float) samplesNumber, ADXL345Settings.samplingFrequency);
            samplesInCurrentSecond = samplesNumber - samplesInFullShownSeconds;
            invisibleSeconds = samplesInFullShownSeconds / ADXL345Settings.samplingFrequency - LAST_SECONDS_TO_SHOW;
        }
        //Draw descriptions
        for (int i = 0; i <= 5; i++) {
            String description = String.valueOf(invisibleSeconds + i) + " s";
            float yPosition = canvas.getHeight() / 2 + 25;
            float xPosition = (i * ADXL345Settings.samplingFrequency - samplesInCurrentSecond) * pixelsPerPeriod;
            canvas.drawText(description, xPosition, yPosition, mTextPaint);

        }
    }

    /**
     * Draw bounds around Graph
     *
     * @param canvas
     */
    private void drawBounds(Canvas canvas) {
        canvas.drawRect(0, -canvas.getHeight() / 2, canvas.getWidth(), canvas.getHeight() / 2, mBoundsPaint);
    }
}
