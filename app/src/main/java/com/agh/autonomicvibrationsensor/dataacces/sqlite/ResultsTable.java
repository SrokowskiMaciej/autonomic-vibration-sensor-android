package com.agh.autonomicvibrationsensor.dataacces.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * SQL table for staring results from all axes of parent measurement
 */
public class ResultsTable {
    static final String TABLE_RESULTS = "results";
    public static final String COLUMN_ID = "_id";
    //Id of measurement to which given result belong
    public static final String COLUMN_MEASUREMENT_ID = "measurement_id";
    //Result from x axis
    public static final String COLUMN_X = "x";
    //Result from y axis
    public static final String COLUMN_Y = "y";
    //Result from z axis
    public static final String COLUMN_Z = "z";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_RESULTS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_MEASUREMENT_ID + " integer not null, "
            + COLUMN_X + " real not null, "
            + COLUMN_Y + " real not null, "
            + COLUMN_Z + " real not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(ResultsTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULTS);
        onCreate(database);
    }
}
