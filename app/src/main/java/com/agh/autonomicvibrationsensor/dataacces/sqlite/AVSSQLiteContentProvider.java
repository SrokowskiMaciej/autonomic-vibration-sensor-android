package com.agh.autonomicvibrationsensor.dataacces.sqlite;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

/**
 * Content provider for saving data from measurements
 */
public class AVSSQLiteContentProvider extends ContentProvider {

    //SQLiteOpenHelper instance
    private AVSSQLiteOpenHelper database;

    //Uri numbers
    private static final int MEASUREMENTS = 10;
    private static final int MEASUREMENTS_ID = 20;
    private static final int RESULTS = 30;
    private static final int RESULTS_ID = 40;

    //Authority of content provider, has to be also specified in AndroidManifest.xml
    private static final String AUTHORITY = "com.agh.autonomicvibrationsensor.sqlitecontentprovider";

    //Hierarchy of data
    private static final String BASE_PATH_MEASUREMENTS = "measurements";
    private static final String BASE_PATH_RESULTS = "measurements/results";

    //Uris of this content provider
    public static final Uri CONTENT_URI_MEASUREMENTS = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_MEASUREMENTS);
    public static final Uri CONTENT_URI_RESULTS = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH_RESULTS);

    //Uri matcher
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    //Static setup of uriMatcher
    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_MEASUREMENTS, MEASUREMENTS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_MEASUREMENTS + "/#", MEASUREMENTS_ID);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_RESULTS, RESULTS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH_RESULTS + "/#", RESULTS_ID);
    }


    public AVSSQLiteContentProvider() {
    }

    @Override
    public boolean onCreate() {
        //initializing of SqliteOpenHelper
        database = new AVSSQLiteOpenHelper(getContext());
        return false;
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int uriType = sURIMatcher.match(uri);
        long id;
        Uri uriOut;
        switch (uriType) {
            case MEASUREMENTS:
                id = sqlDB.insert(MeasurementTable.TABLE_MEASUREMENT, null, values);
                uriOut = Uri.withAppendedPath(CONTENT_URI_MEASUREMENTS, String.valueOf(id));
                break;
            case RESULTS:
                id = sqlDB.insert(ResultsTable.TABLE_RESULTS, null, values);
                uriOut = Uri.withAppendedPath(CONTENT_URI_RESULTS, String.valueOf(id));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return uriOut;
    }


    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        String id;
        int rowsDeleted = 0;
        switch (uriType) {
            case MEASUREMENTS:
                rowsDeleted = sqlDB.delete(MeasurementTable.TABLE_MEASUREMENT, selection,
                        selectionArgs);
                break;
            case MEASUREMENTS_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(MeasurementTable.TABLE_MEASUREMENT,
                            MeasurementTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(MeasurementTable.TABLE_MEASUREMENT,
                            MeasurementTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;
            case RESULTS:
                rowsDeleted = sqlDB.delete(ResultsTable.TABLE_RESULTS, selection,
                        selectionArgs);
                break;
            case RESULTS_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(ResultsTable.TABLE_RESULTS,
                            ResultsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(ResultsTable.TABLE_RESULTS,
                            ResultsTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }


    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        Cursor cursor;
        SQLiteDatabase db = database.getWritableDatabase();
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case MEASUREMENTS:
                queryBuilder.setTables(MeasurementTable.TABLE_MEASUREMENT);
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case MEASUREMENTS_ID:
                queryBuilder.setTables(MeasurementTable.TABLE_MEASUREMENT);
                queryBuilder.appendWhere(MeasurementTable.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case RESULTS:
                queryBuilder.setTables(ResultsTable.TABLE_RESULTS);
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case RESULTS_ID:
                queryBuilder.setTables(ResultsTable.TABLE_RESULTS);
                queryBuilder.appendWhere(ResultsTable.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                cursor = queryBuilder.query(db, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }


        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;

    }


    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated;
        String id;
        switch (uriType) {
            case MEASUREMENTS:
                rowsUpdated = sqlDB.update(MeasurementTable.TABLE_MEASUREMENT, values, selection,
                        selectionArgs);
                break;
            case MEASUREMENTS_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(MeasurementTable.TABLE_MEASUREMENT, values,
                            MeasurementTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(MeasurementTable.TABLE_MEASUREMENT, values,
                            MeasurementTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;
            case RESULTS:
                rowsUpdated = sqlDB.update(ResultsTable.TABLE_RESULTS, values, selection,
                        selectionArgs);
                break;
            case RESULTS_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(ResultsTable.TABLE_RESULTS, values,
                            ResultsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(ResultsTable.TABLE_RESULTS, values,
                            ResultsTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs
                    );
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

}
