package com.agh.autonomicvibrationsensor.dataacces.sqlite;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * SQL table to store data about saved measurements
 */
public class MeasurementTable {
    static final String TABLE_MEASUREMENT = "measurements";
    public static final String COLUMN_ID = "_id";
    //Datetime of taking measurement
    public static final String COLUMN_DATETIME = "datetime";
    //Frequency with which measurement was taken
    public static final String COLUMN_FREQUENCY = "frequency";
    //Table create clause
    private static final String DATABASE_CREATE = "create table "
            + TABLE_MEASUREMENT
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_DATETIME + " text not null, "
            + COLUMN_FREQUENCY + " integer not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(MeasurementTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_MEASUREMENT);
        onCreate(database);
    }
}

