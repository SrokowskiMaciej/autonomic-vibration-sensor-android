package com.agh.autonomicvibrationsensor.dataacces.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * SQLiteOpenHelper managing underlying database
 */
public class AVSSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "noise.db";
    private static final int DATABASE_VERSION = 1;

    public AVSSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        MeasurementTable.onCreate(sqLiteDatabase);
        ResultsTable.onCreate(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        MeasurementTable.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
        ResultsTable.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }
}
