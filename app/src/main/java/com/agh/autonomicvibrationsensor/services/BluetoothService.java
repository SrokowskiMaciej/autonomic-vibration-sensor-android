package com.agh.autonomicvibrationsensor.services;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.agh.autonomicvibrationsensor.activities.DeviceListActivity;
import com.agh.autonomicvibrationsensor.activities.MeasurementActivity;
import com.agh.autonomicvibrationsensor.model.ADXL345Settings;
import com.agh.autonomicvibrationsensor.model.Axis;
import com.agh.autonomicvibrationsensor.model.Measurement;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Service in which all communication by bluetooth with accelerometer module is done
 */
public class BluetoothService extends Service {

    //Binder that Activity has to retrieve in order to get reference to this service
    private final IBinder mBinder = new BluetoothServiceBinder();
    // Exceptions tag
    private static final String TAG = "BluetoothService";
    //Argument for retrieving bluetooth device extra from intent in onStartCommand()
    public final static String BLUETOOTH_DEVICE_ARG = "bluetoothDevice";
    // Unique UUID for this application
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //Thread used for connecting to certain device
    private ConnectThread mConnectThread;
    //Thread used for maintaining connection with certain device
    private ConnectedThread mConnectedThread;
    //Measurement to which service will write data retrieved from bluetooth device
    private Measurement measurement = new Measurement();
    //Current state
    private BLUETOOTH_CONNECTION_STATE mState;

    // Enum connection states
    public enum BLUETOOTH_CONNECTION_STATE {
        CONNECTED,// we're doing nothing
        CONNECTING, // now initiating an outgoing connection
        NONE;  // now connected to a remote device
    }

    /**
     * Binder to this service
     */
    public class BluetoothServiceBinder extends Binder {
        public BluetoothService getService() {
            return BluetoothService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mState = BLUETOOTH_CONNECTION_STATE.NONE;

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Try to connect to device obtained from intent extras
        connect((BluetoothDevice) intent.getParcelableExtra(BLUETOOTH_DEVICE_ARG));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Set the current state of connection
     *
     * @param state New state of connection
     */
    private synchronized void setState(BLUETOOTH_CONNECTION_STATE state) {
        mState = state;
    }

    /**
     * Return the current connection state.
     *
     * @return Current state of connection
     */
    public synchronized BLUETOOTH_CONNECTION_STATE getState() {
        return mState;
    }


    /**
     * Set measurement to which service should write results. It is used to obtain data from service
     * while service is writing them from bluetooth connection.
     *
     * @param measurement New measurement
     */
    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }


    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     */
    public synchronized void connect(BluetoothDevice device) {
        // Cancel any thread attempting to make a connection
        if (mState == BLUETOOTH_CONNECTION_STATE.CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(BLUETOOTH_CONNECTION_STATE.CONNECTING);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }


        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Intent intent = new Intent(DeviceListActivity.CONNECTED_ACTION);
        intent.putExtra(DeviceListActivity.BLUETOOTH_DEVICE_BROADCAST_ARG, device);
        sendBroadcast(intent);
        setState(BLUETOOTH_CONNECTION_STATE.CONNECTED);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        // Send a failure message back to the Activity
        Intent intent = new Intent(DeviceListActivity.CONNECTION_FAILED_ACTION);
        sendBroadcast(intent);
        stopSelf();
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        // Send a failure message back to the Activity
        Intent intent = new Intent(MeasurementActivity.CONNECTION_LOST_ACTION);
        sendBroadcast(intent);
    }

    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        setState(BLUETOOTH_CONNECTION_STATE.NONE);
    }

    /*
    Used to send message to vibration sensor to start sending data thru bluetooth
     */
    public void sendStartRecordingMessage() {
        write(ADXL345Settings.startMessage);
    }

    /*
        Used to send message to vibration sensor to stop sending data thru bluetooth
         */
    public void sendStopRecordingMessage() {
        write(ADXL345Settings.stopMessage);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    private void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != BLUETOOTH_CONNECTION_STATE.CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }


    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e(TAG, "create() failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            setName("ConnectThread");

            // Always cancel discovery because it will slow down a connection
            //mAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                connectionFailed();
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "unable to close() socket during connection failure", e2);
                }
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothService.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] inputBuffer = new byte[1024];
            int inputBufferSize = 0;
            byte[] currentSampleBuffer = new byte[24];
            int currentSampleSize = 0;
            Axis currentlyReadAxis = Axis.UNSPECIFIED;


            // Keep listening to the InputStream while connected
            while (true) {
                try {

                    inputBufferSize = mmInStream.read(inputBuffer);
                    //Iterate thru buffer and decide to which axis given sample belongs
                    for (int i = 0; i < inputBufferSize; i++) {
                        switch ((char) inputBuffer[i]) {
                            case 'x':
                                if (currentSampleSize > 0)
                                    addSample(Integer.valueOf(new String(currentSampleBuffer, 0, currentSampleSize)), currentlyReadAxis);
                                currentSampleSize = 0;
                                currentlyReadAxis = Axis.X;
                                break;
                            case 'y':
                                if (currentSampleSize > 0)
                                    addSample(Integer.valueOf(new String(currentSampleBuffer, 0, currentSampleSize)), currentlyReadAxis);
                                currentSampleSize = 0;
                                currentlyReadAxis = Axis.Y;
                                break;
                            case 'z':
                                if (currentSampleSize > 0)
                                    addSample(Integer.valueOf(new String(currentSampleBuffer, 0, currentSampleSize)), currentlyReadAxis);
                                currentSampleSize = 0;
                                currentlyReadAxis = Axis.Z;
                                break;
                            default:
                                currentSampleBuffer[currentSampleSize] = inputBuffer[i];
                                currentSampleSize++;
                        }
                    }
                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                    connectionLost();
                    break;
                }
            }
        }

        /**
         * Adds sample to given axis of current measurement
         *
         * @param sample Sample to add
         * @param axis   Axis
         */
        private void addSample(int sample, Axis axis) {
            switch (axis) {
                case X:
                    measurement.addXAxisSample(ADXL345Settings.getValueInMetersOnSquareSecond(sample));
                    break;
                case Y:
                    measurement.addYAxisSample(ADXL345Settings.getValueInMetersOnSquareSecond(sample));
                    break;
                case Z:
                    measurement.addZAxisSample(ADXL345Settings.getValueInMetersOnSquareSecond(sample));
                    break;
                case UNSPECIFIED:
                    measurement.addZAxisSample(ADXL345Settings.getValueInMetersOnSquareSecond(sample));
                    break;
            }
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);


            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        /**
         * Close connection
         */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }
}
