package com.agh.autonomicvibrationsensor.activities;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.agh.autonomicvibrationsensor.adapters.MeasurementsArrayAdapters;
import com.agh.autonomicvibrationsensor.dataacces.sqlite.AVSSQLiteContentProvider;
import com.agh.autonomicvibrationsensor.dataacces.sqlite.MeasurementTable;
import com.agh.autonomicvibrationsensor.model.Measurement;

import java.util.ArrayList;

/**
 * Activity listing all saved measurements
 */
public class SavedMeasurementsListActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        startSavedMeasurementActivity(((MeasurementsArrayAdapters) l.getAdapter()).getItem(position));
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, AVSSQLiteContentProvider.CONTENT_URI_MEASUREMENTS, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, final Cursor cursor) {

        //Create array of measurements to show
        final ArrayList<Measurement> measurements = new ArrayList<Measurement>();
        //write measurements to array
        while (cursor.moveToNext()) {
            Measurement measurement = new Measurement();
            measurement.setId(cursor.getLong(cursor.getColumnIndex(MeasurementTable.COLUMN_ID)));
            measurement.setDate(cursor.getString(cursor.getColumnIndex(MeasurementTable.COLUMN_DATETIME)));
            measurement.setFrequency(cursor.getInt(cursor.getColumnIndex(MeasurementTable.COLUMN_FREQUENCY)));
            measurements.add(measurement);
        }
        //Move cursor to starting position in case it needs to be reloaded
        cursor.moveToPosition(-1);
//Show list of measurements in listView
        getListView().post(new Runnable() {
            @Override
            public void run() {
                setListAdapter(new MeasurementsArrayAdapters(SavedMeasurementsListActivity.this, android.R.layout.simple_list_item_1, measurements));
            }
        });
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Start Activity to show measurement
     *
     * @param measurement Chosen measurement
     */
    private void startSavedMeasurementActivity(Measurement measurement) {
        Intent intent = new Intent(this, SavedMeasurementActivity.class);
        //Put extras into intent
        intent.putExtra(SavedMeasurementActivity.MEASUREMENT_ID_ARG, measurement.getId());
        intent.putExtra(SavedMeasurementActivity.MEASUREMENT_DATE_ARG, measurement.getDate());
        intent.putExtra(SavedMeasurementActivity.MEASUREMENT_FREQUENCY_ARG, measurement.getFrequency());
        startActivity(intent);
    }


}
