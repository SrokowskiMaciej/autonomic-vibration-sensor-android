package com.agh.autonomicvibrationsensor.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.agh.autonomicvibrationsensor.R;
import com.agh.autonomicvibrationsensor.dataacces.sqlite.AVSSQLiteContentProvider;
import com.agh.autonomicvibrationsensor.dataacces.sqlite.MeasurementTable;
import com.agh.autonomicvibrationsensor.dataacces.sqlite.ResultsTable;
import com.agh.autonomicvibrationsensor.model.ADXL345Settings;
import com.agh.autonomicvibrationsensor.model.Axis;
import com.agh.autonomicvibrationsensor.model.Measurement;
import com.agh.autonomicvibrationsensor.services.BluetoothService;
import com.agh.autonomicvibrationsensor.surfeceviews.DynamicGraphSurfaceView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Activity receiving data from measurement
 */
public class MeasurementActivity extends Activity implements ActionBar.TabListener, View.OnClickListener {
    // Key names received from the BluetoothChatService Handler
    public static final String BLUETOOTH_DEVICE_ARG = "deviceArg";

    //Action of broadcast received after connection with device has been lost
    public static final String CONNECTION_LOST_ACTION = "connectionLostAction";

    // Layout Views
    private DynamicGraphSurfaceView mSurfaceView;
    private Button mStartButton, mSaveButton, mRejectButton;
    private TextView stateTextView;
    private Measurement mMeasurement;

    //Progress bar
    private ProgressDialog mProgressDialog;

    //Bluetooth Service
    private BluetoothService mService;

    //Bluetooth device
    private BluetoothDevice mBluetoothDevice;

    //Measuring flag
    private boolean isMeasuring = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement);
        setupActivity();

    }

    private void setupActivity() {

        //Get bluetooth device extra
        mBluetoothDevice = getIntent().getParcelableExtra(BLUETOOTH_DEVICE_ARG);
        //Setup ActionBar and tabs
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setTitle(mBluetoothDevice.getName());
        actionBar.setLogo(R.drawable.ic_bluetooth_connected);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.addTab(actionBar.newTab().setText("X").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Y").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Z").setTabListener(this));

        //Setup views
        stateTextView = (TextView) findViewById(R.id.stateTextView);
        mStartButton = (Button) findViewById(R.id.startStopButton);
        mStartButton.setOnClickListener(this);
        mSaveButton = (Button) findViewById(R.id.saveButton);
        mSaveButton.setOnClickListener(this);
        mRejectButton = (Button) findViewById(R.id.rejectButton);
        mRejectButton.setOnClickListener(this);
        mSurfaceView = (DynamicGraphSurfaceView) findViewById(R.id.surfaceView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Determine if Bluetooth service is running. If no show connection lost message.
        if (!isServiceRunning(BluetoothService.class)) {
            mService = null;
            showConnectionLost();
        } else {
            //If yes bind to service and show connected message
            bindService(new Intent(this, BluetoothService.class), mConnection, 0);
            showConnected();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        //Register broadcasts for detecting if connection to the device has been lost
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTION_LOST_ACTION);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        //Unregister broadcasts for detecting if connection to the device has been lost
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Determine if service is running and if yes unbind from it and stop it
        if (isServiceRunning(BluetoothService.class)) {
            mService.stop();
            unbindService(mConnection);
            stopService(new Intent(this, BluetoothService.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Show connected message
     */
    private void showConnected() {
        stateTextView.setText(getResources().getString(R.string.activity_measurement_connected_text));
        stateTextView.setTextColor(getResources().getColor(R.color.theme_color));
    }

    /**
     * Show connection lost message
     */
    private void showConnectionLost() {
        stateTextView.setText(getResources().getString(R.string.activity_measurement_connection_lost_text));
        stateTextView.setTextColor(getResources().getColor(R.color.error_color));
    }

    /**
     * Show measuring message
     */
    private void showMeasuring() {
        stateTextView.setText(getResources().getString(R.string.activity_measurement_measuring_text));
        stateTextView.setTextColor(getResources().getColor(R.color.correct_color));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startStopButton:
                if (!isMeasuring) {
                    mStartButton.setText(getResources().getString(R.string.activity_measurement_stop_text));
                    showMeasuring();
                    startMeasurement();
                } else {
                    mStartButton.setText(getResources().getString(R.string.activity_measurement_start_text));
                    showConnected();
                    stopMeasurement();
                }
                break;
            case R.id.saveButton:
                saveResults(mMeasurement);
                showSavingOptions(false);
                break;
            case R.id.rejectButton:
                mSurfaceView.setMeasurement(new Measurement());
                showSavingOptions(false);
                break;
        }

    }

    /**
     * Here we set axis from which we should show results. Before check if surfaceView is not null.
     * This is because this method fires on the creation of activity and before surfaceVIew has been initialized
     */
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        switch (tab.getPosition()) {
            case 0:
                if (mSurfaceView != null)
                    mSurfaceView.setAxis(Axis.X);
                break;
            case 1:
                if (mSurfaceView != null)
                    mSurfaceView.setAxis(Axis.Y);

                break;
            case 2:
                if (mSurfaceView != null)
                    mSurfaceView.setAxis(Axis.Z);

                break;
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }


    /**
     * This method is used for starting new mesurement
     */
    private void startMeasurement() {
        if (mService.getState() != BluetoothService.BLUETOOTH_CONNECTION_STATE.CONNECTED) {
            return;
        }
        isMeasuring = true;
        //Initialize new Measurement and initialize its arrays
        mMeasurement = new Measurement();
        mMeasurement.initializeArrays();
        //Initialize date for setting measurement date
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        mMeasurement.setDate(dateFormat.format(date));
        //Set measurement to service and surfaceView so they can exchange data
        mService.setMeasurement(mMeasurement);
        mSurfaceView.setMeasurement(mMeasurement);
        //Send message to start receiving data
        mService.sendStartRecordingMessage();
        showMeasuring();
    }

    /*
        This method is used for stoping measurement in progress
         */
    private void stopMeasurement() {
        if (mService.getState() != BluetoothService.BLUETOOTH_CONNECTION_STATE.CONNECTED) {
            return;
        }
        mService.sendStopRecordingMessage();
        showConnected();
        isMeasuring = false;
        showSavingOptions(true);
    }


    /**
     * Utility method used for determining if service is running
     *
     * @param serviceClass Class of service we want to check if is running
     * @return true if service is running, false otherwise
     */
    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method used for saving measurement and results to content provider
     *
     * @param measurement Measurement from which we want to save data
     */
    private void saveResults(final Measurement measurement) {
        //Show progress bar
        showProgressBar(measurement);
        //Make new thread to saving data asynchronously
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //Save measurement
                long measurementId = ContentUris.parseId(saveMeasurement(measurement));
                //Determine array with least size so there won't be any null pointer exception in case of different array sizes.
                int sizeOfSmallestArray = measurement.getXAxisResults().size();
                if (measurement.getYAxisResults().size() < sizeOfSmallestArray)
                    sizeOfSmallestArray = measurement.getYAxisResults().size();
                if (measurement.getZAxisResults().size() < sizeOfSmallestArray)
                    sizeOfSmallestArray = measurement.getZAxisResults().size();
                //Save data
                ContentValues contentValues = new ContentValues();
                synchronized (measurement.getLock()) {
                    for (int i = 0; i < sizeOfSmallestArray; i++) {
                        contentValues.put(ResultsTable.COLUMN_MEASUREMENT_ID, measurementId);
                        contentValues.put(ResultsTable.COLUMN_X, measurement.getXAxisResults().get(i));
                        contentValues.put(ResultsTable.COLUMN_Y, measurement.getYAxisResults().get(i));
                        contentValues.put(ResultsTable.COLUMN_Z, measurement.getZAxisResults().get(i));
                        getContentResolver().insert(AVSSQLiteContentProvider.CONTENT_URI_RESULTS, contentValues);
                    }
                }
                //After data was saved, hide progress bar from UI Thread and set new measurement to surfaceView
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressBar();
                        mSurfaceView.setMeasurement(new Measurement());
                    }
                });
            }
        });
        thread.start();

    }

    /**
     * Saves measurement in content provider
     *
     * @param measurement Measurement we want to save
     * @return Returns id of saved measurement
     */
    private Uri saveMeasurement(Measurement measurement) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MeasurementTable.COLUMN_FREQUENCY, ADXL345Settings.samplingFrequency);
        contentValues.put(MeasurementTable.COLUMN_DATETIME, measurement.getDate());
        return getContentResolver().insert(AVSSQLiteContentProvider.CONTENT_URI_MEASUREMENTS, contentValues);
    }

    /**
     * Shows progress bar while saving data to content provider
     *
     * @param measurement Measurement we want to show we are saving
     */
    private void showProgressBar(Measurement measurement) {
        String title = getResources().getString(R.string.activity_measurement_saving_title_dialog_text);
        String message = getResources().getString(R.string.activity_measurement_saving_message_dialog_text) + measurement.getDate();
        mProgressDialog = ProgressDialog.show(this, title, message);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
    }


    /**
     * Hides progress bar
     */
    private void hideProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    /**
     * Toggles visibility of start measurement and saving options buttons
     *
     * @param show
     */
    private void showSavingOptions(boolean show) {
        if (show) {
            mSaveButton.setVisibility(View.VISIBLE);
            mRejectButton.setVisibility(View.VISIBLE);
            mStartButton.setVisibility(View.INVISIBLE);
        } else {
            mSaveButton.setVisibility(View.INVISIBLE);
            mRejectButton.setVisibility(View.INVISIBLE);
            mStartButton.setVisibility(View.VISIBLE);
        }
    }


    /**
     * Connection required to bind service to Activity
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder binder) {
            BluetoothService.BluetoothServiceBinder b = (BluetoothService.BluetoothServiceBinder) binder;
            mService = b.getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
        }
    };

    /**
     * Broadcast receiver to listen for connection lost broadcast
     */
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            showConnectionLost();
            mStartButton.setEnabled(false);
            mService.stop();
            unbindService(mConnection);
            stopService(new Intent(MeasurementActivity.this, BluetoothService.class));
            mService = null;
            if (isMeasuring)
                showSavingOptions(true);
            isMeasuring = false;
        }
    };


}