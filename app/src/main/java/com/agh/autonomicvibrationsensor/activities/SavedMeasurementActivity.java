package com.agh.autonomicvibrationsensor.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.view.Menu;
import android.view.MenuItem;

import com.agh.autonomicvibrationsensor.R;
import com.agh.autonomicvibrationsensor.dataacces.sqlite.AVSSQLiteContentProvider;
import com.agh.autonomicvibrationsensor.dataacces.sqlite.ResultsTable;
import com.agh.autonomicvibrationsensor.model.Axis;
import com.agh.autonomicvibrationsensor.model.Measurement;
import com.agh.autonomicvibrationsensor.surfeceviews.StaticGraphSurfaceView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Activity showing selected previously saved measurement
 */
public class SavedMeasurementActivity extends Activity implements ActionBar.TabListener, LoaderManager.LoaderCallbacks<Cursor> {

    //Shown measurement
    private Measurement mMeasurement = new Measurement();
    //SurefaceView to show measurement on
    private StaticGraphSurfaceView mSurfaceView;
    //Progress bar
    private ProgressDialog mProgressDialog;

    //Arguments of measurement data
    public static final String MEASUREMENT_ID_ARG = "measurementId";
    public static final String MEASUREMENT_FREQUENCY_ARG = "measurementFrequency";
    public static final String MEASUREMENT_DATE_ARG = "measurementDate";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_measurement);
        //Get measurement passed from list of saved measurements
        mMeasurement.setDate(getIntent().getStringExtra(MEASUREMENT_DATE_ARG));
        mMeasurement.setFrequency(getIntent().getIntExtra(MEASUREMENT_FREQUENCY_ARG, 0));
        mMeasurement.setId(getIntent().getLongExtra(MEASUREMENT_ID_ARG, 0));
        //Initialize action bar
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setTitle(mMeasurement.getDate());
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.addTab(actionBar.newTab().setText("X").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Y").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Z").setTabListener(this));
        actionBar.setDisplayHomeAsUpEnabled(true);
        //Initialize surfaceView
        mSurfaceView = (StaticGraphSurfaceView) findViewById(R.id.surfaceView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Start loading data
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.saved_measurement, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            //Respond to action mail button
            case R.id.action_mail:
                sendFile(writeToFile(mMeasurement.getDate(), getStringOfResults(mMeasurement)));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Here we set axis from which we should show results. Before check if surfaceView is not null.
     * This is because this method fires on the creation of activity and before surfaceVIew has been initialized
     */
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        switch (tab.getPosition()) {
            case 0:
                if (mSurfaceView != null)
                    mSurfaceView.setAxis(Axis.X);
                break;
            case 1:
                if (mSurfaceView != null)
                    mSurfaceView.setAxis(Axis.Y);
                break;
            case 2:
                if (mSurfaceView != null)
                    mSurfaceView.setAxis(Axis.Z);
                break;
        }

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    /**
     * Make string to be saved to sent file
     *
     * @param measurement Measurement to be saved
     * @return Formatted String with measurement contents
     */
    private String getStringOfResults(Measurement measurement) {
        //Number of blank spaces between columns of data
        int spacesPerColumn = 15;
        //Substring to get blank spaces from
        String blankSpaces = "                                     ";
        //String builder for building final string
        StringBuilder stringBuilder = new StringBuilder();
        //Write frequency
        stringBuilder.append("frequency: ");
        stringBuilder.append(String.valueOf(measurement.getFrequency()));
        stringBuilder.append("\n \n ");
        //Write columns descriptions
        stringBuilder.append("X");
        stringBuilder.insert(stringBuilder.length(), blankSpaces, 0, spacesPerColumn - 1);
        stringBuilder.append("Y");
        stringBuilder.insert(stringBuilder.length(), blankSpaces, 0, spacesPerColumn - 1);
        stringBuilder.append("X");
        stringBuilder.insert(stringBuilder.length(), blankSpaces, 0, spacesPerColumn - 1);
        stringBuilder.append("\n");
        //Get lock from measurement to read data
        synchronized (measurement.getLock()) {
            for (int i = 0; i < measurement.getXAxisResults().size(); i++) {
                //Insert every value from x,y,z axes and blank spaces between them
                String xValue = String.valueOf(measurement.getXAxisResults().get(i));
                stringBuilder.append(xValue);
                stringBuilder.insert(stringBuilder.length(), blankSpaces, 0, spacesPerColumn - xValue.length());
                String yValue = String.valueOf(measurement.getYAxisResults().get(i));
                stringBuilder.append(yValue);
                stringBuilder.insert(stringBuilder.length(), blankSpaces, 0, spacesPerColumn - yValue.length());
                String zValue = String.valueOf(measurement.getZAxisResults().get(i));
                stringBuilder.append(zValue);
                stringBuilder.insert(stringBuilder.length(), blankSpaces, 0, spacesPerColumn - zValue.length());
                stringBuilder.append("\n");
            }
        }
        //return built string
        return stringBuilder.toString();
    }

    /**
     * Writes to txt file
     *
     * @param fileName Name of the file to write to
     * @param content  Contents of file
     * @return Written file
     */
    private File writeToFile(String fileName, String content) {
        try {
            FileOutputStream fos = openFileOutput(fileName + ".txt", Context.MODE_PRIVATE);
            fos.write(content.getBytes());
            fos.close();
            return new File(getFilesDir(), fileName + ".txt");

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sends file by e-mail
     *
     * @param inFile File to send
     */
    private void sendFile(final File inFile) {
        if (inFile == null)
            return;
        // let the FileProvider generate an URI for this private file
        final Uri uri = FileProvider.getUriForFile(this, "com.agh.autonomicvibrationsensor.activities.SavedMeasurementActivity", inFile);
        // create an intent, so the user can choose which application he/she wants to use to share this file
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        this.startActivity(intent);
    }

    /**
     * Shows progress bar indicating sending operation
     */
    private void showSendingProgressBar() {
        String title = getResources().getString(R.string.activity_saved_measurement_sending_title_text);
        String message = getResources().getString(R.string.activity_saved_measurement_sending_message_text) + mMeasurement.getDate();
        mProgressDialog = ProgressDialog.show(this, title, message);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
    }

    /**
     * Shows progress bar indicating loading operation
     */
    private void showLoadingProgressBar() {
        String title = getResources().getString(R.string.activity_saved_measurement_loading_title_text);
        String message = getResources().getString(R.string.activity_saved_measurement_loading_message_text) + mMeasurement.getDate();
        mProgressDialog = ProgressDialog.show(this, title, message);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
    }

    /**
     * Hides all progress bars
     */
    private void hideProgressBar() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        showLoadingProgressBar();
        //Create loader with data from given measurement
        return new CursorLoader(this, AVSSQLiteContentProvider.CONTENT_URI_RESULTS, null, ResultsTable.COLUMN_MEASUREMENT_ID + "=?", new String[]{String.valueOf(mMeasurement.getId())}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //Initialize arrays of measurement
        mMeasurement.initializeArrays();
        //Write data to measurement
        while (data.moveToNext()) {
            mMeasurement.addXAxisSample(data.getFloat(data.getColumnIndex(ResultsTable.COLUMN_X)));
            mMeasurement.addYAxisSample(data.getFloat(data.getColumnIndex(ResultsTable.COLUMN_Y)));
            mMeasurement.addZAxisSample(data.getFloat(data.getColumnIndex(ResultsTable.COLUMN_Z)));
        }
        //Move cursor position to 0 in case it needs to be reloaded
        data.moveToPosition(-1);
        //Show results on surfaceView
        mSurfaceView.post(new Runnable() {
            @Override
            public void run() {
                mSurfaceView.setMeasurement(mMeasurement);
                hideProgressBar();
            }
        });

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
