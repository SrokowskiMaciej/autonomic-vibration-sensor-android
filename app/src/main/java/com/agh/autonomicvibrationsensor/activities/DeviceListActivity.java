package com.agh.autonomicvibrationsensor.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.agh.autonomicvibrationsensor.R;
import com.agh.autonomicvibrationsensor.adapters.BluetoothDevicesArrayAdapter;
import com.agh.autonomicvibrationsensor.services.BluetoothService;

import java.util.ArrayList;
import java.util.Set;

/*
In this Activity user may search for new devices and choose which one they want to connect to
 */
public class DeviceListActivity extends Activity {


    //Connect broadcast receiver actions
    public static final String CONNECTION_FAILED_ACTION = "connectionFailedAction";
    public static final String CONNECTED_ACTION = "connectedAction";

    //Connect broadcast receiver, device argument
    public static final String BLUETOOTH_DEVICE_BROADCAST_ARG = "device";

    //Progress dialog used when searching new devices and connecting to one
    private ProgressDialog mRequestDialog;
    // Intent extra. Used for enabling bluetooth
    private static final int REQUEST_ENABLE_BT = 2;

    // Member fields
    private BluetoothAdapter mBtAdapter;
    private BluetoothDevicesArrayAdapter mPairedDevicesArrayAdapter;
    private BluetoothDevicesArrayAdapter mNewDevicesArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mDiscoverDevicesReceiver, filter);
    }

    private void setupActivity() {


        // Initialize the button to perform device discovery
        Button scanButton = (Button) findViewById(R.id.button_scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                doDiscovery();
            }
        });

        // Initialize array adapters. One for already paired devices and
        // one for newly discovered devices
        mPairedDevicesArrayAdapter = new BluetoothDevicesArrayAdapter(this, android.R.layout.simple_list_item_1, new ArrayList<BluetoothDevice>());
        mNewDevicesArrayAdapter = new BluetoothDevicesArrayAdapter(this, android.R.layout.simple_list_item_1, new ArrayList<BluetoothDevice>());

        // Find and set up the ListView for paired devices
        ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

        // Find and set up the ListView for newly discovered devices
        ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
        newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(mDeviceClickListener);

        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices.size() > 0) {
            findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
            for (BluetoothDevice device : pairedDevices) {
                mPairedDevicesArrayAdapter.add(device);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        // setupActivity() will then be called during onActivityResult
        if (!mBtAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            setupActivity();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Register broadcasts for getting the results of connection try
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTION_FAILED_ACTION);
        intentFilter.addAction(CONNECTED_ACTION);
        registerReceiver(mConnectionBroadcastReceiver, intentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Register broadcasts for getting the results of connection try
        unregisterReceiver(mConnectionBroadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }

        // Unregister discover devices broadcast listeners
        unregisterReceiver(mDiscoverDevicesReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    Here we get results from request to enable bluetooth
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupActivity();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, R.string.activity_device_list_bt_not_enabled_leaving_toast_text, Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    /**
     * Start device discovery with the BluetoothAdapter
     */
    private void doDiscovery() {
        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }
        showScanningProgressBar();
        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    // The on-click listener for all devices in the ListViews
    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            // Cancel discovery because it's costly and we're about to connect
            if (mBtAdapter.isDiscovering())
                mBtAdapter.cancelDiscovery();

            // Start service and put bluetooth device
            BluetoothDevice bluetoothDevice = ((BluetoothDevicesArrayAdapter) adapterView.getAdapter()).getItem(position);
            Intent intent = new Intent(getApplicationContext(), BluetoothService.class);
            intent.putExtra(BluetoothService.BLUETOOTH_DEVICE_ARG, bluetoothDevice);
            getApplicationContext().startService(intent);
            showConnectionProgressBar(bluetoothDevice);
        }
    };


    /*
    Here we show progress bar while we are waiting for BluetoothService to connect to selected device
     */
    public void showConnectionProgressBar(BluetoothDevice bluetoothDevice) {
        String title = getResources().getString(R.string.activity_device_list_connecting_title_dialog_text);
        String message = getResources().getString(R.string.activity_device_list_connecting_message_dialog_text) + bluetoothDevice.getName();
        mRequestDialog = ProgressDialog.show(this, title, message);
        mRequestDialog.setIndeterminate(true);
        mRequestDialog.setCancelable(false);
    }

    /*
    Here we show progress bar while we are discovering new devices
     */
    public void showScanningProgressBar() {
        String title = getResources().getString(R.string.activity_device_list_scanning_title_dialog_text);
        String message = getResources().getString(R.string.activity_device_list_scanning_message_dialog_text);
        mRequestDialog = ProgressDialog.show(this, title, message);
        mRequestDialog.setIndeterminate(true);
        mRequestDialog.setCancelable(true);
        mRequestDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (mBtAdapter.isDiscovering())
                    mBtAdapter.cancelDiscovery();
            }
        });
    }


    /*
    Here we are hiding progress bar regardless of if we were scanning for new devices or connecting to one.
     */
    public void hideProgressBar() {
        if (mRequestDialog != null && mRequestDialog.isShowing())
            mRequestDialog.dismiss();
    }


    private void connectionFailed() {
        hideProgressBar();
        Toast.makeText(DeviceListActivity.this, getResources().getString(R.string.activity_device_list_connection_failed_toast_text), Toast.LENGTH_SHORT).show();
    }

    /*
    Show information about connect and start MeasurementActivity with bluetoothDevice argument
     */
    private void connected(BluetoothDevice bluetoothDevice) {
        hideProgressBar();
        Toast.makeText(DeviceListActivity.this, getResources().getString(R.string.activity_device_list_connected_to_toast_text) + bluetoothDevice.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MeasurementActivity.class);
        intent.putExtra(MeasurementActivity.BLUETOOTH_DEVICE_ARG, bluetoothDevice);
        startActivity(intent);
    }

    // The BroadcastReceiver that listens for discovered devices
    private final BroadcastReceiver mDiscoverDevicesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    mNewDevicesArrayAdapter.add(device);
                }
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                hideProgressBar();
                if (mNewDevicesArrayAdapter.getCount() == 0) {
                    String noDevices = getResources().getText(R.string.activity_device_list_none_found_toast_text).toString();
                    Toast.makeText(DeviceListActivity.this, noDevices, Toast.LENGTH_SHORT).show();
                }
            }
        }
    };


    /*
    Broadcast receiver getting broadcasts about status of connection.
     */
    public BroadcastReceiver mConnectionBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CONNECTION_FAILED_ACTION))
                connectionFailed();
            else if (intent.getAction().equals(CONNECTED_ACTION)) {
                connected((BluetoothDevice) intent.getParcelableExtra(BLUETOOTH_DEVICE_BROADCAST_ARG));
            }
        }
    };

}