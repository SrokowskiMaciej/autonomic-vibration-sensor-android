package com.agh.autonomicvibrationsensor.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.agh.autonomicvibrationsensor.R;


public class MainActivity extends Activity implements View.OnClickListener {

    private Button newMeasurementButton, savedMeasurementsButton, exitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        newMeasurementButton = (Button) findViewById(R.id.newMeasurementButton);
        savedMeasurementsButton = (Button) findViewById(R.id.savedMeasurementsButton);
        exitButton = (Button) findViewById(R.id.exitButton);
        newMeasurementButton.setOnClickListener(this);
        savedMeasurementsButton.setOnClickListener(this);
        exitButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.newMeasurementButton:
                startDeviceListActivity();
                break;
            case R.id.savedMeasurementsButton:
                startSavedMeasurementsListActivity();
                break;
            case R.id.exitButton:
                finish();
                break;
        }

    }

    private void startDeviceListActivity() {
        Intent intent = new Intent(this, DeviceListActivity.class);
        startActivity(intent);
    }

    private void startSavedMeasurementsListActivity() {
        Intent intent = new Intent(this, SavedMeasurementsListActivity.class);
        startActivity(intent);
    }
}
