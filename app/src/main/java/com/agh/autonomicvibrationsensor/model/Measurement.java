package com.agh.autonomicvibrationsensor.model;

import gnu.trove.list.array.TFloatArrayList;

/**
 * Class representing measurement. Before using you have to call initializeArrays() method. This is
 * done for sake of performance because for new results to be delivered faster from bluetooth connection
 * we need already initialized array that do not has to increase its size on the run.
 */
public class Measurement {

    /**
     * Lock. Any object using results from this measurement needs to acquire it and synchronize any
     * operations done on them.
     */
    private final Object lock = new Object();
    //Date on which measurement was taken
    private String date = "";
    //Id of measurement obtained after saving it to content provider
    private long id;
    //Frequency with which measurement was taken
    private int frequency;
    //List of X axis results. This is list of primitives from Trove library for sake of optimization
    private TFloatArrayList xAxisResults = new TFloatArrayList();
    //List of Y axis results. This is list of primitives from Trove library for sake of optimization
    private TFloatArrayList yAxisResults = new TFloatArrayList();
    //List of Z axis results. This is list of primitives from Trove library for sake of optimization
    private TFloatArrayList zAxisResults = new TFloatArrayList();
    //Highest absolute X axis result ever added to measurement
    private float absHighestXAxisSample = 0.0f;
    //Highest absolute Y axis result ever added to measurement
    private float absHighestYAxisSample = 0.0f;
    //Highest absolute Z axis result ever added to measurement
    private float absHighestZAxisSample = 0.0f;

    /**
     * This method has to be called before you try to write any results to arrays. This is
     * done for sake of performance because for new results to be delivered faster from bluetooth connection
     * we need already initialized array that do not has to increase its size on the run.
     */
    public void initializeArrays() {
        xAxisResults = new TFloatArrayList(100000);
        yAxisResults = new TFloatArrayList(100000);
        zAxisResults = new TFloatArrayList(100000);
    }

    /**
     * Returns Lock object for this measurement. Any object using results from this measurement needs
     * to acquire it and synchronize any operations done on them.
     *
     * @return Lock
     */
    public Object getLock() {
        return lock;
    }

    /**
     * Get id of this measurement in content provider
     *
     * @return Returns id.
     */
    public long getId() {
        return id;
    }

    /**
     * Set id of this measurement after saving it to content provider
     *
     * @param id Id in content provider
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get date on which this measurement was taken
     *
     * @return date
     */
    public String getDate() {
        return date;
    }

    /**
     * Set date on which this measurement was taken
     *
     * @param date Date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Get frequency with which this measurement was taken
     *
     * @return frequency in Hz.
     */
    public int getFrequency() {
        return frequency;
    }

    /**
     * Set frequency with which this measurement was taken
     *
     * @param frequency in Hz
     */
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    /**
     * Get results of X axis. Any operations, including read, on results should be done in synchronized
     * block done on lock object
     *
     * @return X axis results
     */
    public TFloatArrayList getXAxisResults() {
        return xAxisResults;
    }

    /**
     * Get results of Y axis. Any operations, including read, on results should be done in synchronized
     * block done on lock object
     *
     * @return Y axis results
     */
    public TFloatArrayList getYAxisResults() {
        return yAxisResults;
    }

    /**
     * Get results of Z axis. Any operations, including read, on results should be done in synchronized
     * block done on lock object
     *
     * @return Z axis results
     */
    public TFloatArrayList getZAxisResults() {
        return zAxisResults;
    }

    /**
     * Get highest absolute X axis result ever added to measurement
     *
     * @return Highest result
     */
    public float getAbsHighestXAxisSample() {
        return absHighestXAxisSample;
    }

    /**
     * Get highest absolute Y axis result ever added to measurement
     *
     * @return Highest result
     */
    public float getAbsHighestYAxisSample() {
        return absHighestYAxisSample;
    }

    /**
     * Get highest absolute Z axis result ever added to measurement
     *
     * @return Highest result
     */
    public float getAbsHighestZAxisSample() {
        return absHighestZAxisSample;
    }

    /**
     * Add sample to X axis. This is done in synchronized manner and is synchronized against
     * lock you can obtain by getLock() method
     *
     * @param xAxisSample Sample in m/s^2
     */
    public void addXAxisSample(float xAxisSample) {
        float absSample = Math.abs(xAxisSample);
        if (absSample > absHighestXAxisSample)
            absHighestXAxisSample = absSample;
        synchronized (lock) {
            xAxisResults.add(xAxisSample);
        }
    }

    /**
     * Add sample to Y axis. This is done in synchronized manner and is synchronized against
     * lock you can obtain by getLock() method
     *
     * @param yAxisSample Sample in m/s^2
     */
    public void addYAxisSample(float yAxisSample) {
        float absSample = Math.abs(yAxisSample);
        if (absSample > absHighestYAxisSample)
            absHighestYAxisSample = absSample;
        synchronized (lock) {
            yAxisResults.add(yAxisSample);
        }
    }

    /**
     * Add sample to Z axis. This is done in synchronized manner and is synchronized against
     * lock you can obtain by getLock() method
     *
     * @param zAxisSample Sample in m/s^2
     */
    public void addZAxisSample(float zAxisSample) {
        float absSample = Math.abs(zAxisSample);
        if (absSample > absHighestZAxisSample)
            absHighestZAxisSample = absSample;
        synchronized (lock) {
            zAxisResults.add(zAxisSample);
        }
    }
}
