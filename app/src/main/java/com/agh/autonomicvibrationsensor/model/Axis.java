package com.agh.autonomicvibrationsensor.model;

/**
 * Enum representing axes
 */
public enum Axis {
    X, Y, Z, UNSPECIFIED;
}
