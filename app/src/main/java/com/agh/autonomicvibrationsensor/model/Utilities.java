package com.agh.autonomicvibrationsensor.model;

/**
 * Utilities class
 */
public class Utilities {
    public static int roundUpToNearest(float from, int to) {
        return (int) (((from - 1) / to) + 1) * to;
    }

    public static int roundDownToNearest(float from, int to) {
        return (int) (from / to) * to;
    }
}
