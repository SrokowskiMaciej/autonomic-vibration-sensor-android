package com.agh.autonomicvibrationsensor.model;

/**
 * Settings of ADXL345 module. Must be the same as currently used module
 */
public class ADXL345Settings {
    public final static float g = 9.80665f;
    //Range of results in g i.e. 8 means range from -4g to 4g
    public final static int gRange = 8;
    //Range in m/s^2
    public final static float range = g * gRange;
    //Resolution of module
    public final static int bits = 10;
    //Number of quants on given range
    public final static double quants = Math.pow(2, bits);
    //Byte array that has to be send to module to start receiving data
    public final static byte[] startMessage = new byte[]{'a'};
    //Byte array that has to be send to module to stop receiving data
    public final static byte[] stopMessage = new byte[]{'s'};
    //Sampling frequency of the module
    public final static int samplingFrequency = 200;
    //Sampling period of the module
    public final static float samplingPeriod = ((float) samplingFrequency) / 100.0f;

    //Utility method for calculating results from two bytes to m/s^2
    public static float getValueInMetersOnSquareSecond(int level) {
        return ((float) level / (float) quants) * range;
    }
}
