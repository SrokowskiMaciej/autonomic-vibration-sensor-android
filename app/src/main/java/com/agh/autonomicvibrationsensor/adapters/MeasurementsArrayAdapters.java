package com.agh.autonomicvibrationsensor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.agh.autonomicvibrationsensor.model.Measurement;

import java.util.List;


/**
 * Adapter for listing saved measurements
 */
public class MeasurementsArrayAdapters extends ArrayAdapter<Measurement> {
    public MeasurementsArrayAdapters(Context context, int resource, List<Measurement> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView rowView = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        Measurement measurement = getItem(position);
        rowView.setText(measurement.getDate());
        return rowView;
    }
}