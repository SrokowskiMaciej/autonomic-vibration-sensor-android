package com.agh.autonomicvibrationsensor.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Adapter for listing bluetooth devices
 */

public class BluetoothDevicesArrayAdapter extends ArrayAdapter<BluetoothDevice> {
    public BluetoothDevicesArrayAdapter(Context context, int resource, List<BluetoothDevice> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView rowView = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        BluetoothDevice bluetoothDevice = getItem(position);
        rowView.setText(bluetoothDevice.getName() + "\n" + bluetoothDevice.getAddress());
        return rowView;
    }
}
